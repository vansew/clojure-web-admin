
* Description

Clojure web admin is a clojure(script)-based, metadata-driven web admin system providing some generic features in web admin system. 

The core features are as follows:

 + CURD with data-Level permissions based on korma
 + Menu tree & toolbars with button-level permissions based on reagent/re-frame
 + High-level abstractions in field type based on reagent-forms (e.g. image, attachment, enum, select, etc.) 
 + Font-end & Back-end validation based on bouncer.


Other features:

 + Login/Logout
 + Optimistic lock for updating data
 + Default value showing in forms
 + Export as excel file/Import from excel file
 + Bar chart for datagrid based on CanvasJs

More features are coming up..

* Quick Start

This project already has some build-in moduals including 'User', 'Role', 'Organization', which can be used
as common moduals in your new project. 
'Computer' and 'Brand' moduals are only  for demostration.

Let`s get start!


+ Clone the project

  #+BEGIN_SRC 
    git clone https://github.com/b1412/clojure-web-admin.git
  #+END_SRC

  #+BEGIN_SRC shell
    cd clojure-web-admin
  #+END_SRC

+ Execute SQL script in MySql

  #+BEGIN_SRC shell
   mysql -uroot -proot
   
   mysql> source db/demo.sql;
  #+END_SRC
   
+ Start project
 
    start font-end

    #+BEGIN_SRC clojure
      lein figwheel
    #+END_SRC

    start back-end

    #+BEGIN_SRC clojure
      lein run
    #+END_SRC
  
  NOTE:  to get better experience, use REPL to play!

+ Visit http://localhost:3000 and log in as admin 
   
    username: admin  
    
    password: admin

+ Go [[https://github.com/b1412/clojure-web-admin/wiki/Demo-in-details][demo in details]] to see snapshots of core features in demo moduals before you play it.


* More details

  Go [[https://github.com/b1412/clojure-web-admin/wiki/Home][wiki]] to go deeper into the project. 

# License

Copyright © 2015 Leon Zhou

Distributed under the Eclipse Public License, the same as Clojure.
